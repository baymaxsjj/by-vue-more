> [个人博客原文地址](https://www.yunmobai.cn/blog/11)
### 项目介绍
多平台发布工具，支持简书，csdn等平台，本人只测试过Csdn,其它平台未经测试，请自行修改，未经许可，不得商用！ 如果你觉得有用的话，就给一个star吧,如果你不需要多端发布功能，也可以当一个本地的记事本！所有数据均保存在本地！本项目无后台！
### 项目演示
[项目代码](https://gitee.com/baymaxsjj/by-vue-more)
[项目演示](http://more.yunmobai.cn/)
<table>
  <tbody>
    <tr>
      <td align="center" valign="middle">
          <img src="https://img-blog.csdnimg.cn/20201028214656933.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTI5NDYwNw==,size_16,color_FFFFFF,t_70#pic_center" >
      </td>
      <td align="center" valign="middle">
          <img src="https://img-blog.csdnimg.cn/20201028214726450.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTI5NDYwNw==,size_16,color_FFFFFF,t_70#pic_center" >
      </td>
    </tr>
     <tr>
      <td align="center" valign="middle">
          <img src="https://img-blog.csdnimg.cn/20201028214749805.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTI5NDYwNw==,size_16,color_FFFFFF,t_70#pic_center" >
      </td>
      <td align="center" valign="middle">
          <img src="https://img-blog.csdnimg.cn/2020102821490893.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTI5NDYwNw==,size_16,color_FFFFFF,t_70#pic_center" >
      </td>
    </tr>
  </tbody>
</table>


### 实现原理
通过几次的使用openwrite，发现其实现原理就是我上面的两个条件，不过它有解决办法而已，那让我们来了解其原理。使用过的都知道，就是通过插件来实现跨域获取cookie,也通过插件完成文章发布一系列功能。这个插件就是对外的一个桥梁！还好他的插件是源码，没有编译，研究了一番，这个插件至暴露一个对外的沟通的方法，其它的都是对不同平台的请求头做一些设置。而我就是通过这个插件实现多平台发布功能！
### 使用方法
下载该平台的插件，更改manifest.json中配置，本地可以不要改，但是现在别的域名下使用还需更改！将给文件中所有的openwrite.cn改成自己的域名！如图
![插件配置](https://img-blog.csdnimg.cn/20201028194633681.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTI5NDYwNw==,size_16,color_FFFFFF,t_70#pic_center)
然后就可以在浏览器中安装插件了！
### 跟官网插件保持同步更新，防止部分平台失效！
上面我也说过了！openwrite插件都是从官网获取，我们打开开发者工具找到Application->Local Storage将其中数字的放到我项目的public->config.txt中覆盖旧的代码，
![获取代码](https://img-blog.csdnimg.cn/20201028200218277.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTI5NDYwNw==,size_16,color_FFFFFF,t_70#pic_center)
### 修改项目代码
由于我只测试了Csdn,其它平台可能有问题，你只要把对于平台的修改就可以了！如果像添加其它平台的那就把其中demo.vue复制一下，完成该平台的配置，
在config.txt 中添加几个方法，研究一下就知道了那几个方法了！
![修改代码](https://img-blog.csdnimg.cn/20201028200626618.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTI5NDYwNw==,size_16,color_FFFFFF,t_70#pic_center)

### 集成到自己的博客或项目
本来像把这个项目搞错一个npm 插件的！不过没接触，也没时间研究了！如果你想集成到自己的项目中只有把其中的代码原封不动的搞到自己项目中，不过挺费事，最好还是用我这个项目本地运行吧！这是我博客后台的！这是我前期在我博客中写的！有些丑！后来我把它拿出来单独写了！就成了这个项目
![集成到博客](https://img-blog.csdnimg.cn/20201029070242140.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTI5NDYwNw==,size_16,color_FFFFFF,t_70#pic_center)
> 注：本项目是用于学习和掌握新知识而编写，不得用于商业用途！
> 代码写的水平不太好！请谅解！自己学习的Vue才几个月，也没接触过真正的项目！代码逻辑都是自己想的！因为没有提前思考过，代码有些臃肿！如果你有好的建议，可以到我的博客留下留言 ，或者在我发布平台下评论！

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
