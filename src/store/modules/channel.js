function dateFormat(fmt, date) {
    let ret;
    const opt = {
        "Y+": date.getFullYear().toString(), // 年
        "m+": (date.getMonth() + 1).toString(), // 月
        "d+": date.getDate().toString(), // 日
        "H+": date.getHours().toString(), // 时
        "M+": date.getMinutes().toString(), // 分
        "S+": date.getSeconds().toString() // 秒
            // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (let k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
        }
    }
    return fmt;
}
const channel = {
    state: {
        configs: {

        },
        articles: [],
        id: 0,
        userConfigs: {}
    },
    getters: {
        // ...
        getArticle: (state) => (id) => {
            console.log(id)
            const article = state.articles.find(todo => todo.id === parseInt(id))
            console.log(article)
            if (!article) {
                return false
            }
            return article
        },
        getArticles: (state) => (page, pageSize) => {
            // const articles = state.articles
            // console.log(state.articles.reverse())
            if (page == 0) {
                return state.articles
            }
            let array = [...state.articles].reverse()
            let offset = (page - 1) * pageSize;
            return (offset + pageSize >= array.length) ? array.slice(offset, array.length) : array.slice(offset, offset + pageSize);
        },
        getConfig: (state) => (name) => {
            return state.configs[name]
        },

        getUserConfig: (state) => {
            return state.userConfigs
        },
        userConfig: (state) => (name) => {
            return state.userConfigs[name]
        },
        getTags: state => {
            let tags = []
            state.articles.forEach(item => {
                tags = tags.concat(item.tags)
            })
            return Array.from(new Set(tags))
        },
        getCategorys: state => {
            let categorys = []
            state.articles.forEach(item => {
                categorys = categorys.concat(item.category)
            })
            return Array.from(new Set(categorys))
        }
    },
    mutations: {
        setDesc(state, data) { //写入token
            const name = data.name
            state.desc[name] = data
                // state.desc.push(data)
        },
        setId(state, data) {
            state.id = data
        },
        setArticle(state, data) {
            const leng = state.articles.length
            let id = 1
            if (leng > 0) {
                id = parseInt(state.articles[leng - 1].id) + 1
            }
            data['id'] = id
            const date = new Date()
            data['createTime'] = dateFormat("YYYY-mm-dd HH:MM:SS", date)
            data['updateTime'] = dateFormat("YYYY-mm-dd HH:MM:SS", date)
            state.articles.push(data)
        },
        updateUrl(state, data) {
            console.log('更新链接', data)
            for (let i in state.articles) {
                console.log(i)
                if (state.articles[i].id == state.id && state.articles[i].channels[data.channel]) {
                    console.log('正在更新链接', state.articles[i].channels[data.channel])
                    state.articles[i].channels[data.channel].url = data.url
                    break
                }
            }
        },
        updateArticle(state, data) {
            let id = data.id
            data['updateTime'] = dateFormat("YYYY-mm-dd HH:MM:SS", new Date())
            state.articles.forEach((item, index) => {
                if (parseInt(item.id) == id) {
                    state.articles[index] = data
                }
            })
        },
        setConfig(state, data) {
            state.configs[data.channel] = data
        },
        setDescConfig(state, data) {
            state.configs[data.channel].enable = true
            state.configs[data.channel].config = data.config
        },
        setUserConfig(state, data) {
            state.userConfigs[data.channel] = data
        },
        updateConfig(state, data) {
            const name = data.channel
            Object.keys(data).forEach(function(key) {
                state.configs[name][key] = data[key]
            })
        },
        setStatu(state, data) {
            const name = data.name
            state.configs[name].enable = data.value
            console.log(state)
        },
    },
}
export default channel