"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function dateFormat(fmt, date) {
  var ret;
  var opt = {
    "Y+": date.getFullYear().toString(),
    // 年
    "m+": (date.getMonth() + 1).toString(),
    // 月
    "d+": date.getDate().toString(),
    // 日
    "H+": date.getHours().toString(),
    // 时
    "M+": date.getMinutes().toString(),
    // 分
    "S+": date.getSeconds().toString() // 秒
    // 有其他格式化字符需求可以继续添加，必须转化成字符串

  };

  for (var k in opt) {
    ret = new RegExp("(" + k + ")").exec(fmt);

    if (ret) {
      fmt = fmt.replace(ret[1], ret[1].length == 1 ? opt[k] : opt[k].padStart(ret[1].length, "0"));
    }
  }

  return fmt;
}

var channel = {
  state: {
    configs: {},
    articles: [],
    id: 0,
    userConfigs: {}
  },
  getters: {
    // ...
    getArticle: function getArticle(state) {
      return function (id) {
        console.log(id);
        var article = state.articles.find(function (todo) {
          return todo.id === parseInt(id);
        });
        console.log(article);

        if (!article) {
          return false;
        }

        return article;
      };
    },
    getArticles: function getArticles(state) {
      return function (page, pageSize) {
        // const articles = state.articles
        // console.log(state.articles.reverse())
        if (page == 0) {
          return state.articles;
        }

        var array = _toConsumableArray(state.articles).reverse();

        var offset = (page - 1) * pageSize;
        return offset + pageSize >= array.length ? array.slice(offset, array.length) : array.slice(offset, offset + pageSize);
      };
    },
    getConfig: function getConfig(state) {
      return function (name) {
        return state.configs[name];
      };
    },
    getUserConfig: function getUserConfig(state) {
      return state.userConfigs;
    },
    userConfig: function userConfig(state) {
      return function (name) {
        return state.userConfigs[name];
      };
    },
    getTags: function getTags(state) {
      var tags = [];
      state.articles.forEach(function (item) {
        tags = tags.concat(item.tags);
      });
      return Array.from(new Set(tags));
    },
    getCategorys: function getCategorys(state) {
      var categorys = [];
      state.articles.forEach(function (item) {
        categorys = categorys.concat(item.category);
      });
      return Array.from(new Set(categorys));
    }
  },
  mutations: {
    setDesc: function setDesc(state, data) {
      //写入token
      var name = data.name;
      state.desc[name] = data; // state.desc.push(data)
    },
    setId: function setId(state, data) {
      state.id = data;
    },
    setArticle: function setArticle(state, data) {
      var leng = state.articles.length;
      var id = 1;

      if (leng > 0) {
        id = parseInt(state.articles[leng - 1].id) + 1;
      }

      data['id'] = id;
      var date = new Date();
      data['createTime'] = dateFormat("YYYY-mm-dd HH:MM:SS", date);
      data['updateTime'] = dateFormat("YYYY-mm-dd HH:MM:SS", date);
      state.articles.push(data);
    },
    updateUrl: function updateUrl(state, data) {
      console.log('更新链接', data);

      for (var i in state.articles) {
        console.log(i);

        if (state.articles[i].id == state.id && state.articles[i].channels[data.channel]) {
          console.log('正在更新链接', state.articles[i].channels[data.channel]);
          state.articles[i].channels[data.channel].url = data.url;
          break;
        }
      }
    },
    updateArticle: function updateArticle(state, data) {
      var id = data.id;
      data['updateTime'] = dateFormat("YYYY-mm-dd HH:MM:SS", new Date());
      state.articles.forEach(function (item, index) {
        if (parseInt(item.id) == id) {
          state.articles[index] = data;
        }
      });
    },
    setConfig: function setConfig(state, data) {
      state.configs[data.channel] = data;
    },
    setDescConfig: function setDescConfig(state, data) {
      state.configs[data.channel].enable = true;
      state.configs[data.channel].config = data.config;
    },
    setUserConfig: function setUserConfig(state, data) {
      state.userConfigs[data.channel] = data;
    },
    updateConfig: function updateConfig(state, data) {
      var name = data.channel;
      Object.keys(data).forEach(function (key) {
        state.configs[name][key] = data[key];
      });
    },
    setStatu: function setStatu(state, data) {
      var name = data.name;
      state.configs[name].enable = data.value;
      console.log(state);
    }
  }
};
var _default = channel;
exports["default"] = _default;