import { createStore } from 'vuex'
import createPersistedState from "vuex-persistedstate";
import channel from './modules/channel'
// const c = createPersistedState({
//     paths: ['channel.configs', 'channel.articles'],

// })
export default createStore({
    modules: {
        channel
    },
    plugins: [createPersistedState()],
})