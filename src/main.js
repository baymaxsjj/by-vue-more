import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VMdEditor from '@kangc/v-md-editor';
import '@kangc/v-md-editor/lib/style/base-editor.css';
import githubTheme from '@kangc/v-md-editor/lib/theme/github.js';
import axios from 'axios'
import VueAxios from 'vue-axios'

VMdEditor.use(githubTheme);
import {
    Button,
    message,
    notification,
    Menu,
    Layout,
    Table,
    Avatar,
    Switch,
    Alert,
    Spin,
    Form,
    Input,
    Select,
    Row,
    Col,
    Empty,
    Pagination,
    Modal,
    Steps,
    Drawer,
    Divider,
    Radio,
    Checkbox,
    Popover
} from 'ant-design-vue';
const app = createApp(App)
app.use(VMdEditor);
app.use(VueAxios, axios)
app.use(store)
app.use(router)
app.use(Button)
app.use(message)
app.use(notification)
app.use(Menu)
app.use(Layout)
app.use(Table)
app.use(Avatar)
app.use(Switch)
app.use(Alert)
app.use(Spin)
app.use(Form)
app.use(Input)
app.use(Select)
app.use(Row)
app.use(Col)
app.use(Empty)
app.use(Pagination)
app.use(Modal)
app.use(Steps)
app.use(Drawer)
app.use(Divider)
app.use(Radio)
app.use(Checkbox)
app.use(Popover)
    // app.use(Option)
app.config.globalProperties.$message = message;
app.config.globalProperties.$notification = notification;
app.mount('#app')