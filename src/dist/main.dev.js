"use strict";

var _vue = require("vue");

var _App = _interopRequireDefault(require("./App.vue"));

var _router = _interopRequireDefault(require("./router"));

var _store = _interopRequireDefault(require("./store"));

var _vMdEditor = _interopRequireDefault(require("@kangc/v-md-editor"));

require("@kangc/v-md-editor/lib/style/base-editor.css");

var _github = _interopRequireDefault(require("@kangc/v-md-editor/lib/theme/github.js"));

var _axios = _interopRequireDefault(require("axios"));

var _vueAxios = _interopRequireDefault(require("vue-axios"));

var _antDesignVue = require("ant-design-vue");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_vMdEditor["default"].use(_github["default"]);

var app = (0, _vue.createApp)(_App["default"]);
app.use(_vMdEditor["default"]);
app.use(_vueAxios["default"], _axios["default"]);
app.use(_store["default"]);
app.use(_router["default"]);
app.use(_antDesignVue.Button);
app.use(_antDesignVue.message);
app.use(_antDesignVue.notification);
app.use(_antDesignVue.Menu);
app.use(_antDesignVue.Layout);
app.use(_antDesignVue.Table);
app.use(_antDesignVue.Avatar);
app.use(_antDesignVue.Switch);
app.use(_antDesignVue.Alert);
app.use(_antDesignVue.Spin);
app.use(_antDesignVue.Form);
app.use(_antDesignVue.Input);
app.use(_antDesignVue.Select);
app.use(_antDesignVue.Row);
app.use(_antDesignVue.Col);
app.use(_antDesignVue.Empty);
app.use(_antDesignVue.Pagination);
app.use(_antDesignVue.Modal);
app.use(_antDesignVue.Steps);
app.use(_antDesignVue.Drawer);
app.use(_antDesignVue.Divider);
app.use(_antDesignVue.Radio);
app.use(_antDesignVue.Checkbox);
app.use(_antDesignVue.Popover); // app.use(Option)

app.config.globalProperties.$message = _antDesignVue.message;
app.config.globalProperties.$notification = _antDesignVue.notification;
app.mount('#app');