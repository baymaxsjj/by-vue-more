import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [{
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/channel',
        name: 'Channel',
        component: () =>
            import ('../views/Channel.vue')
    },
    {
        path: '/article/list',
        name: 'Article',
        component: () =>
            import ('../views/Article.vue')
    },
    {
        path: '/article/add',
        component: () =>
            import ('../views/ArticleAdd.vue')
    },
    {
        path: '/article/update/:id',
        name: 'Update',
        component: () =>
            import ('../views/ArticleAdd.vue')
    },
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router